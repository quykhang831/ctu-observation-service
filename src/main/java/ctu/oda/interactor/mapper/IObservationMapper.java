package ctu.oda.interactor.mapper;

import ctu.oda.dto.ObservationDTO;
import ctu.oda.dto.SensorRecordDTO;
import ctu.oda.model.Observation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "cdi")
public interface IObservationMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "dataStreamId", source = "sensorRecord.dataStreamId")
    @Mapping(target = "result", source = "sensorRecord.result")
    @Mapping(target = "resultTime", source = "resultTime", qualifiedByName = "setDefaultResultTime")
    Observation toObservation(SensorRecordDTO sensorRecord, Date resultTime);

    @Named("setDefaultResultTime")
    default Date setDefaultResultTime(Date resultTime) {
        return resultTime != null ? resultTime : Date.from(Instant.now());
    }

    @Named("mapSensorRecords")
    default List<Observation> mapSensorRecords(ObservationDTO observationDTO) {
        return observationDTO.getSensorRecords().stream()
                .map(sensorRecord -> toObservation(sensorRecord, observationDTO.getResultTime()))
                .collect(Collectors.toList());
    }

    @Named("toObservations")
    default List<Observation> toObservations(ObservationDTO observationDTO) {
        return mapSensorRecords(observationDTO);
    }
}
