package ctu.oda.interactor.in;

import ctu.oda.dto.ObservationDTO;
import ctu.oda.model.Observation;

import java.util.Date;
import java.util.List;

public interface IObservationService {
    void addObservation(ObservationDTO observation);
    Observation findObservationById(String observationId);
    List<Observation> findObservationsIsSortedByTime();
    List<Observation> findObservationsByDataStreamId(String dataStreamId);
    List<Observation> findObservationsByDataStreamIdAndTimeRange(String dataStreamId, Date startTime, Date endTime);
    List<Observation> findLatestObservationsByDataStreamIds(List<String> dataStreamId);
    void deleteObservation(String id);
}
