package ctu.oda.model;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import lombok.*;
import org.bson.types.ObjectId;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Observation extends PanacheMongoEntity {
    public ObjectId id; // used by MongoDB for the _id field
    public String dataStreamId;
    public String result;
    public Date resultTime;
}
