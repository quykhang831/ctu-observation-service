package ctu.oda.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SensorRecordDTO {
    private String dataStreamId;
    private String result;
}
