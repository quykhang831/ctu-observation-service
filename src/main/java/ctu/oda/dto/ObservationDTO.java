package ctu.oda.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ObservationDTO {
    private List<SensorRecordDTO> sensorRecords;
    private Date resultTime;
}
