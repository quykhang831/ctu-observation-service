package ctu.oda.boundary;

import ctu.oda.dto.ObservationDTO;
import ctu.oda.model.Message;
import ctu.oda.model.Observation;
import ctu.oda.service.ObservationService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Path("/observations")
public class ObservationResource {
    private final ObservationService observationService;
    private static final SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Inject
    public ObservationResource(ObservationService observationService) {
        this.observationService = observationService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addObservation(ObservationDTO observation) {
        observationService.addObservation(observation);
        return Response
                .status(Response.Status.CREATED)
                .build();
    }

    @GET
    @Path("/dataStreamId/{dataStreamId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Observation> getAllObservationsByDataStreamId(@PathParam("dataStreamId") String dataStreamId) {
        return observationService.findObservationsByDataStreamId(dataStreamId);
    }

    @GET
    @Path("/dataStreamId/{dataStreamId}/byRange")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Observation> getAllObservationsByDataStreamIdAndTimeRange(
            @PathParam("dataStreamId") String dataStreamId,
            @QueryParam("startTime") String startTime,
            @QueryParam("endTime") String endTime
    ) {
        try {
            Date formattedStartTime = dateTimeFormatter.parse(startTime);
            Date formattedEndTime = dateTimeFormatter.parse(endTime);
            return observationService.findObservationsByDataStreamIdAndTimeRange(dataStreamId, formattedStartTime, formattedEndTime);
        } catch (Exception e) {
            throw new WebApplicationException(Response.status(404).entity(new Message(e.getMessage())).build());
        }
    }

    @POST
    @Path("/dataStreamIds/latest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Observation> getLatestObservationByDataStreamId(List<String> dataStreamIds) {
        try {
            return observationService.findLatestObservationsByDataStreamIds(dataStreamIds);
        } catch (NotFoundException e) {
            throw new WebApplicationException(Response.status(404).entity(new Message(e.getMessage())).build());
        }
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteObservation(@PathParam("id") String id) {
        try {
            observationService.deleteObservation(id);
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("The observation with id " + id + " was deleted")
                    .build();

        } catch (NotFoundException e) {
            throw new WebApplicationException(Response.status(404).entity(e.getMessage()).build());
        }
    }
}
