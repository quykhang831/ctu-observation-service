package ctu.oda.mqtt.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import ctu.oda.dto.ObservationDTO;
import ctu.oda.model.Observation;
import ctu.oda.service.ObservationService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import java.io.IOException;

@ApplicationScoped
public class MqttObservationConsumer {
    private final ObservationService observationService;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Inject
    public MqttObservationConsumer(ObservationService observationService) {
        this.observationService = observationService;
    }


    @Incoming("observations")
    public void consume(byte[] observationData) {
        ObservationDTO observation = new ObservationDTO();
        try {
            observation = objectMapper.readValue(observationData, ObservationDTO.class);
            System.out.println("Received data: " + observation + " | " + observation.getResultTime());
            observationService.addObservation(observation);
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
