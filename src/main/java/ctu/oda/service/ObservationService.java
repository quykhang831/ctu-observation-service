package ctu.oda.service;

import ctu.oda.dto.ObservationDTO;
import ctu.oda.interactor.in.IObservationService;
import ctu.oda.interactor.mapper.IObservationMapper;
import ctu.oda.model.Observation;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Sort;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.NotFoundException;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@ApplicationScoped
public class ObservationService implements IObservationService, PanacheMongoRepository<Observation> {

    @Inject
    IObservationMapper observationMapper;

    @Override
    public void addObservation(ObservationDTO observationDTO) {
        List<Observation> observations = observationMapper.toObservations(observationDTO);
        observations.forEach(this::persist);
    }

    @Override
    public Observation findObservationById(String observationId) {
        Observation observation = find("id", observationId).firstResult();
        if (Objects.isNull(observation)) {
            throw new NotFoundException("Observation with id " + observationId + " does not existed");
        }
        return observation;
    }

    @Override
    public List<Observation> findObservationsIsSortedByTime() {
        return findAll(Sort.descending("resultTime")).list();
    }

    @Override
    public List<Observation> findObservationsByDataStreamId(String dataStreamId) {
        List<Observation> observations = list("dataStreamId = ?1 order by resultTime desc", dataStreamId);
        observations.sort((o1, o2) -> o2.getResultTime().compareTo(o1.getResultTime()));
        return observations;
    }

    @Override
    public List<Observation> findObservationsByDataStreamIdAndTimeRange(String dataStreamId, Date startTime, Date endTime) {
        List<Observation> observations = list("dataStreamId = ?1 and resultTime >= ?2", dataStreamId, startTime.toInstant());
        return observations.stream().filter(observation -> !observation.getResultTime()
                .after(endTime)).sorted((o1, o2) -> o2.getResultTime().compareTo(o1.getResultTime())).collect(Collectors.toList());
    }

    @Override
    public List<Observation> findLatestObservationsByDataStreamIds(List<String> dataStreamIds) {
        return dataStreamIds.stream()
                .map(dataStreamId -> list("dataStreamId = ?1 order by resultTime desc", dataStreamId).stream().min((o1, o2) -> o2.getResultTime().compareTo(o1.getResultTime()))
                        .orElseThrow(() -> new NotFoundException("Don't have any Observation for dataStreamId " + dataStreamId)))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteObservation(String id) {
        Observation observation = findObservationById(id);
        observation.delete();
    }
}
